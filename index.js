require('dotenv').config();
let express = require('express');

let steem = require('steem');
steem.api.setOptions({url: 'https://rpc2.smoke.io'});
steem.config.set('address_prefix', 'SMK');
steem.config.set('chain_id', '1ce08345e61cd3bf91673a47fc507e7ed01550dab841fd9cdb0ab66ef576aaf0');


const NodeCache = require("node-cache");
const myCache = new NodeCache({stdTTL: 3600, checkperiod: 3600});


let app = express();

const port = process.env.PORT || 7856;
const image_proxy_url = "https://smoke.io/smokeimageproxy/64x64/";
const profile_image_default = "https://smoke.io/images/smoke_user.png";

serverStart = () => {
  let router = express.Router();
  // eg., http://localhost:7070/profileimage/baabeetaa
  router.get('/:accountname', async function (req, res) {
    let redirect_url = image_proxy_url + profile_image_default;

    try {
      let profile_image = myCache.get(req.params.accountname);
      if (typeof profile_image !== 'undefined' && profile_image !== null) {
        redirect_url = image_proxy_url + profile_image;
      } else {
        try {
          let accs = await steem.api.getAccountsAsync([req.params.accountname]);

          if (accs.length > 0) {
            let acc = accs[0];
            let json_metadata = acc.json_metadata;
            let md = JSON.parse(json_metadata);

            profile_image = md.profile.profile_image;

            console.log("profile_image=" + profile_image);
            if ((typeof profile_image === 'undefined') || (profile_image === null)) {
              profile_image = profile_image_default;
            }

            // remember to set cache
            myCache.set(req.params.accountname, profile_image);

            redirect_url = image_proxy_url + profile_image;
          }
        } catch (e) {
          // console.log(e.message);

          // also set cache for no-profile-image user, so we dont have to call rpc next time.
          myCache.set(req.params.accountname, profile_image_default);
        }
      }
    } catch (e) {
      // console.log(e.message);
    } finally {
      res.redirect(redirect_url);
    }
  });

  app.use('/profileimage', router);

  app.listen(port);
  console.log('serverStart on port ' + port);
};

serverStart();